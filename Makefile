###############################################################################
#
# @description Commands to setup, develop and deploy the project
# @author Parag M. <admin@bitsofparag.com>
#
###############################################################################
# Make
###############################################################################

# Please add new or update existing variables to .env.development file

# Production variables for both staging and production environments
# are added in the .env.production file.

define get_cur_branch
$(shell git branch | sed -n '/\* /s///p')
endef

define echo_cur_branch
@echo Now on [$(call get_cur_branch)] branch
endef

version_file =
version = 0.1  # TODO read from version file

ifeq ($(ENVIRONMENT), )
include .env.development
export
endif

ifeq ($(ENVIRONMENT), $(filter $(ENVIRONMENT), staging production))
include .env.production
export
endif

.PHONY: help setup dev test lint clean build release

.DEFAULT_GOAL := help

# Help screen
help:
	@echo "======================================================================="
	@echo "    Commands to setup, develop and deploy this project     "
	@echo "=======================================================================\n"
	@echo "make setup                    --> Setup project and install deps"
	@echo "make dev                      --> Run in development mode"
	@echo "make test                     --> Run test"
	@echo "make lint                     --> Run linting"
	@echo "make clean                    --> Clean"
	@echo "\n--------------- Release tasks -----------------"
	@echo "make build                    --> Build"
	@echo "make release                  --> Release"
	@echo "\n--------------- Cloud provisioning tasks -----------------"
	@echo "make setup-cloud              --> Setup cloud infrastructure"
	@echo "make teardown-cloud           --> Tear down cloud infrastructure"
	@echo "make provision-cloud          --> Provision $(ENVIRONMENT) on the cloud"
	@echo "\n====================================================================="
	@echo "=======================================================================\n"


##############################################################################
# Target Rules
##############################################################################
setup:
	@echo "\n==> Setting up project..."

dev:
	env DEV_PORT=$(DEV_PORT) npm run dev

test:
	@echo "\n==> Running tests"

lint:
	@echo "\n==> Running lint..."

clean:
	@echo "\n==> Clean..."
	npm run clean

################################################
#  Release tasks
################################################

build:
	npm run build

release:
	@echo "\n==> Release tasks come here"

################################################
#  Cloud provisioning tasks
################################################
