![Webgl Experiments Logo](./docs/dggl_logo.png)


![PyPI - Python Version](https://img.shields.io/pypi/pyversions/Django.svg)
![node](https://img.shields.io/node/v/:packageName.svg)
[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/:user/:repo.svg)]()
![CRAN/METACRAN](https://img.shields.io/cran/l/devtools.svg)


## Introduction
---------------

Code snippets to play with Webgl

_Keywords: ['webgl', 'experiments']_

## Getting Started
------------------

- Change directory into your newly created project, i.e the project root

  `cd webgl-experiments`

- Update the `.env.development` and `.env.production` files

  As a convention, ports are assigned in the following ranges:

  * Dev ports - [3000...3999]
  * API ports - [4000...4999]
  * Database ports - Depends on the type of database
  * SSH ports for private instances - [6000...6999]
  * Analytics ports - [7000...7999]
  * Blockchain ports - [30000...39999]

- Setup project and install dependencies

  `make setup`

  (optional) Add additional setup instructions here.

- Run project in development mode

   `make dev`

- Run tests.

  `make test`


#### See `make` or `make help` for more

## License
----------
Webgl Experiments is under the MIT license. See the [LICENSE](https://opensource.org/licenses/MIT)
for more information.

## Contributors
---------------

Maintained by Parag M. <admin@bitsofparag.com>
