// More here https://rollupjs.org/guide/en#using-config-files
import replace from 'rollup-plugin-replace';
import postcss from 'rollup-plugin-postcss';
import uglify from 'rollup-plugin-uglify';
import copy from 'rollup-plugin-cpy';

// PostCSS plugins
import simplevars from 'postcss-simple-vars';
import nested from 'postcss-nested';
import postcssPresetEnv from 'postcss-preset-env';
import cssnano from 'cssnano';

export default {
  input: 'src/index.js',
  output: {
    file: 'dist/static/index.js',
    format: 'iife',
    sourceMap: 'inline',
  },
  plugins: [
    copy([
      { files: ['src/images'], dest: './dist/static/images', verbose: true },
      { files: 'src/**/*.mp3', dest: './dist/static/audio', verbose: true },
      { files: 'src/**/*.webm', dest: './dist/static/video', verbose: true },
      { files: 'src/static/*.ico', dest: './dist/static', verbose: true }
    ]),
    replace({
      ENVIRONMENT: JSON.stringify(process.env.ENVIRONMENT || 'development'),
    }),
    (process.env.ENVIRONMENT === 'production' && uglify()),
    postcss({
      plugins: [
        simplevars(),
        nested(),
        postcssPresetEnv({
          stage: 3,
          features: {
            'color-mod-function': { unresolved: 'warn' }
          }
        }),
        cssnano(),
      ],
      extensions: [ '.css' ],
    }),
  ]
};
