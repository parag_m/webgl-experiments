var express = require('express');
var app = express();
const PORT = process.env.PORT;

app.set('view engine', 'ejs');
app.set('views', 'src');

app.use(express.static('dist'));

// index page
app.get('/', function(req, res) {
  res.render('index');
});

app.listen(PORT);
console.log(`${PORT} is the magic port!`);
