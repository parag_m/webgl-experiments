import {XYZ_COORD} from './constants';
import utils from './utils';

// Mutates coords
function moveCoords(coords, options) {
  const coordSystem = options.coordSystem;
  const numOfArrayCoordinates = options.numOfPoints * coordSystem;

  for (var i = 0; i < numOfArrayCoordinates; i += coordSystem) { // grouping of array items by coords
    coords[i] += utils.getRandomDelta();
    coords[i + 1] += utils.getRandomDelta();

    if (coordSystem == XYZ_COORD) {
      coords[i + 2] += utils.getRandomDelta();
    }
  }

  return coords;
}

export default moveCoords;
