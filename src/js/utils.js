const offsetPoints = 0;

function getRandomDelta() {
  return (Math.random() * .01) - 0.005;
}

function getRandom2DCoords(num) {
  let randomCoords = [];
  for (var i = 0; i < num; i++) {
    let coord = getRandom2DCoord();
    randomCoords.push(coord[0]);
    randomCoords.push(coord[1]);
  }

  return randomCoords;
}

function getRandom2DCoord() {
  return [
    Math.random() * 2 - 1, // x
    Math.random() * 2 - 1 // y
    // z will be assigned automatically to 0
  ];
}

export default {
  offsetPoints,

  getRandomDelta,
  getRandom2DCoord,
  getRandom2DCoords,
};
