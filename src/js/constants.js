const CANVAS_WIDTH = 1400;
const CANVAS_HEIGHT = 2000;
const XYZ_COORD = 3;
const XY_COORD = 2;
const COLORS = {
  primary: [1.0, 0.1, 0.2, 1.0]
};

export {
  CANVAS_WIDTH,
  CANVAS_HEIGHT,
  XYZ_COORD,
  XY_COORD,
  COLORS
};
