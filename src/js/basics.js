import getShader from  './get-shader';

function clear(context) {
  context.clearColor(0, 0, 0, 0);
  context.clear(context.COLOR_BUFFER_BIT);
}

function initGL(options) {
  let canvas = document.getElementById("canvas");
  canvas.width = options.CANVAS_WIDTH;
  canvas.height = options.CANVAS_HEIGHT;
  const gl = canvas.getContext("webgl");
  gl.viewport(0, 0, canvas.width, canvas.height);
  gl.clearColor(0, 0, 0, 0);
  return gl;
};


function compileShadersAndProgram(context) {
  // create vertex shader first
  let vertexShader = getShader(context, 'shader-vs');

  // create fragment shader
  let fragmentShader = getShader(context, 'shader-fs');

  // create program
  let shaderProgram = context.createProgram();
  context.attachShader(shaderProgram, vertexShader);
  context.attachShader(shaderProgram, fragmentShader);
  context.linkProgram(shaderProgram);
  let success = context.getProgramParameter(shaderProgram, context.LINK_STATUS);
  if (success) {
    return shaderProgram;
  }

  console.log(context.getProgramInfoLog(shaderProgram));
  context.deleteProgram(shaderProgram);
  return null;
}

// --- Prepare the vertices before executing below ---- //

// create a buffer and bind to ARRAY_BUFFER
function setupArrayBuffer(context) {
  let buffer = context.createBuffer();
  context.bindBuffer(context.ARRAY_BUFFER, buffer);
}

function bindDataToArrayBuffer(context, coords) {
  context.bufferData(
    context.ARRAY_BUFFER,
    new Float32Array(coords),
    context.DYNAMIC_DRAW
  );
}

// Creates vertices (cartesian coords) on the screen
function setupVertices(context, shaderProgram, COORD_SYSTEM, options = {}) {
  let {pointSize, fragColor} = options;
  var coordsAttrib = context.getAttribLocation(shaderProgram, "coords");
  //context.vertexAttrib3f(coordsAttrib, ...data.coords);
  context.vertexAttribPointer(coordsAttrib, COORD_SYSTEM, context.FLOAT, false, 0, 0);
  context.enableVertexAttribArray(coordsAttrib); // looks up the bound buffer
  //context.bindBuffer(context.ARRAY_BUFFER, null);

  var pointSizeAttrib = context.getAttribLocation(shaderProgram, "pointSize");
  context.vertexAttrib1f(pointSizeAttrib, pointSize);

  var color = context.getUniformLocation(shaderProgram, "color");
  context.uniform4f(color, ...fragColor);
}

export {
  initGL,
  clear,
  compileShadersAndProgram,
  setupVertices,
  setupArrayBuffer,
  bindDataToArrayBuffer
};
