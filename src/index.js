// Import styles (automatically injected into <head>).
import './css/main.css';

import {
  CANVAS_WIDTH,
  CANVAS_HEIGHT,
  XY_COORD,
  COLORS
} from './js/constants';
import utils from './js/utils';
import {
  initGL,
  clear,
  compileShadersAndProgram,
  setupVertices,
  setupArrayBuffer,
  bindDataToArrayBuffer } from './js/basics';
import moveCoords from './js/animations';

/* BEGIN Livereload script for Rollup */

if (ENVIRONMENT !== 'production') {
  const head = document.getElementsByTagName('head')[0];
  const script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = 'http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1';
  head.appendChild(script);
}
/* END Livereload script for Rollup */

const gl = initGL({CANVAS_WIDTH, CANVAS_HEIGHT});
const program = compileShadersAndProgram(gl);
gl.useProgram(program);

/* Draw three fixed points or join them with a triangle //
const coords = [
  -0.9, -0.9, 0.0,
  0.9, -0.9, 0.0,
  0.0, 0.9, 0.0
];
const offsetPoints = 0;
const numOfPoints = 3;
setupArrayBuffer(gl);
bindDataToArrayBuffer(gl, coords);
setupVertices(gl, program, XYZ_COORD, {
  pointSize: 2,
  fragColor: COLORS.primary
});
// exec
gl.drawArrays(gl.TRIANGLES, offsetPoints, numOfPoints);
gl.drawArrays(gl.POINTS, offsetPoints, numOfPoints);
/* */

/* Draw many points and animate them //*/
const numOfPoints = 5000;
let coords = utils.getRandom2DCoords(numOfPoints);
setupArrayBuffer(gl);
bindDataToArrayBuffer(gl, coords);
setupVertices(gl, program, XY_COORD, {
  pointSize: 4,
  fragColor: COLORS.primary
});
(function draw() {
  coords = moveCoords(coords, {
    coordSystem: XY_COORD,
    numOfPoints: numOfPoints
  });
  bindDataToArrayBuffer(gl, coords, {drawingType: gl.STATIC_DRAW});
  clear(gl);
  gl.drawArrays(gl.POINTS, utils.offsetPoints, numOfPoints);

  requestAnimationFrame(draw);
})();
/* */
